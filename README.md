# greek cipher                      254 pts



## Description

You think you've seen all of the "classic" ciphers? Instead of your standard cipher, I've created my own cipher: the monoalphagreek cipher!

Answer with just the flag in lowercase with symbols left in.

file : greek.txt

## Analyse

The file contain string of greeks characters. At the end, we can see flag structure. It's seems only aplha chars are affected.

## Method

Because it's mono-alphabetic cypher, we can distingue structure in symbols exactly like letters in words. We can subsitute letter, begining with the knowed char lactf.

Then, the game is to guess each characters. It's really easy because they are lots of words.
